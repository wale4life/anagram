from flask import Flask, request #, render_template
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)


class AnagramApi(Resource):

	def get(self, word='happy'):
		args = request.args
		word = args['word']

		""" Call Anagram Module and process words """
		from anagramEngine import process


		#return {'d':word}
		data = process(word)

		return { 'data': data }



api.add_resource(AnagramApi, '/')


# @app.route('/')
# def hello():
# 	word = request.args.get('word')
# 	return


if __name__ == '__main__':
	app.run(debug=True)

